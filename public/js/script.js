

function togglePopupMenu(popup, shareLabel){

		if (popup[0].style.display == 'block') {
			popup[0].style.display = 'none';
			
		} else if ( popup[0].style.display == 'none' ) {
			popup[0].style.display = 'block';

		} else if ( popup[0].style.display == '' ) {
			popup[0].style.display = 'block';
		}

		let position = (shareLabel[0].offsetWidth / 2) - (popup[0].offsetWidth / 2 ) + "px";

		popup[0].style.left = position;
	}

window.onload = function() {

	/* FOR POPUP */

	let shareLabel = document.getElementsByClassName("share-button");
	let popup = document.getElementsByClassName("social-popup");
	let togglePopup = document.getElementById("togglePopup");
	let popupCloseBtn = document.getElementById("closeBtn");


	togglePopup.addEventListener("click", function(){
		togglePopupMenu(popup, shareLabel);
	});

	popupCloseBtn.addEventListener("click", function(){
		togglePopupMenu(popup, shareLabel);
	});

	/* END POPUP */


	
	/* SLIDER */
	let productList = document.getElementsByClassName("flex-item");
	let sliderBtnLeft = document.getElementById("slider-left");
	let sliderBtnRight = document.getElementById("slider-right");
	
	let currentActiveItem = 0;
	let mobileW = 700;

	if (window.innerWidth < mobileW) {
		for (var i = 0; i < productList.length; i++) {
			if (currentActiveItem == i) {
 				continue;
			}
			productList[i].style.display = 'none';
		}
	}

	sliderBtnLeft.addEventListener("click", function(){
		if (currentActiveItem != 0) {
			productList[currentActiveItem].style.display = 'none';
			currentActiveItem--;
			productList[currentActiveItem].style.display = 'block';
		} 
	});

	sliderBtnRight.addEventListener("click", function(){
		if (currentActiveItem != productList.length-1) {
			productList[currentActiveItem].style.display = 'none';
			currentActiveItem++;
			productList[currentActiveItem].style.display = 'block';
		} 
	});
	/* END SLIDER */
}