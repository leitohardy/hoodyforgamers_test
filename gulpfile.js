
let path = require('path');
let gulp = require('gulp');
let less = require('gulp-less');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');
 
// gulp.task('less', function () {
    gulp.task('default', function() {   
    return gulp.src('./less/**/*.less')
      .pipe(sourcemaps.init())
      .pipe(less({
        paths: [ path.join(__dirname, 'less', 'includes') ]
      }))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./public/css'));
  });
  gulp.task('watch', function() {
    gulp.watch('./less/**/*.less', ['default']);  // Watch all the .less files, then run the less task
  });
